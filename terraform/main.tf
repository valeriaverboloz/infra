// EC2
resource "aws_instance" "ec2" {
  ami                    = "ami-0b0dcb5067f052a63"
  instance_type          = "t2.micro"
  iam_instance_profile   = aws_iam_instance_profile.profile.name
  key_name               = aws_key_pair.default.key_name
  vpc_security_group_ids = [aws_security_group.sg.id]
  user_data_base64       = filebase64("${path.module}/user_data.sh")
}

// Key pair
resource "aws_key_pair" "default" {
  key_name   = "keypair-${var.ENVIRONMENT}"
  public_key = tls_private_key.key.public_key_openssh
}


resource "tls_private_key" "key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
