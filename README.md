## Getting started

This repo is dedicated to create EC2 instance from image container and run java application

## What you can find here?

In this repo you can find terraform config files to create all the necessary infrastructure and then run the application:
- Keys, Security Group for EC2 instance creation
- CloudWatch Log Metric
- "AmazonEC2ContainerRegistryReadOnly" IAM Group
- user_data shell script to run the application and keep the connection

## How to
 
Repete same steps from java-app repo:
- Create new GitLab Project
- Add 3 secrets
- Add new pipeline and then run it

## Pipeline
- In the pipeline you will find multiple stages for terraform run
- Pipeline supports 2 different environments: dev1 and dev2
- Apply and Destroy stages run manually
- You can run both environments in parallel
- tfstate stores in GitLab per environment. 
- The location of tfstate file will be created duting the init stage:
Project -> Infrastructure -> Terraform

## Checking app run

To check that the application is running go to public_instance_ip:8080 in your browser.
